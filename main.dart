import 'package:flutter/material.dart';
import 'splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic App',
      home: SplashAnimation(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        children: <Widget>[
          SizedBox(
            height: 100,
          ),
          Image.asset(
              'C:\Users\Aphiwe\Desktop\MYMTNAPP\ASSESSMENTS\m3App\app1\assets\BUSINESS LOGO.jpg'),
          SizedBox(
            height: 40,
          ),
          Text(
            'CROSSOVER APP LOGIN',
            style: TextStyle(color: Colors.black),
          ),
          SizedBox(
            height: 60,
          ),
          TextField(
            decoration: InputDecoration(
              labelText: "Email Address",
            ),
          ),
          SizedBox(height: 40),
          TextField(
            obscureText: true,
            decoration: InputDecoration(
              labelText: "Password",
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            children: <Widget>[
              ButtonTheme(
                height: 40,
                disabledColor: Colors.blueGrey,
                child: ElevatedButton(
                    child: Text('Login'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProfilePage()),
                      );
                    }),
              ),
            ],
          ),
        ],
      )),
    );
  }
}

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              children: <Widget>[
            Text(
              'PROFILE',
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40.0,
            ),
            TextField(
                decoration: InputDecoration(
                    labelText: 'Name and Surname',
                    hintText: 'Enter name and surname',
                    labelStyle: TextStyle(
                        fontSize: 16,
                        color: Colors.red,
                        fontWeight: FontWeight.bold),
                    hintStyle: TextStyle(
                      fontSize: 13,
                      color: Colors.amberAccent,
                    ))),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Enter Email',
                hintText: 'Enter email address',
              ),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Enter Email',
                hintText: 'Enter email address',
              ),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: 'Enter Cell Number',
                hintText: 'Enter phone number',
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Column(
              children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => PageOne()));
                  },
                  child: Text('Save Info'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pop(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  },
                  child: Text('Login Page'),
                )
              ],
            ),
          ])),
    );
  }
}

class PageOne extends StatefulWidget {
  PageOne({Key? key}) : super(key: key);

  @override
  State<PageOne> createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page One'),
        centerTitle: true,
      ),
      backgroundColor: Colors.green,
      body: SafeArea(
          child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PageTwo(),
                    ));
              },
              child: Text('Next Page'))),
    );
  }
}

class PageTwo extends StatefulWidget {
  PageTwo({Key? key}) : super(key: key);

  @override
  State<PageTwo> createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('page 2'),
      ),
      body: SafeArea(
          child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (context) => PageOne()));
              },
              child: Text('data'))),
    );
  }
}
