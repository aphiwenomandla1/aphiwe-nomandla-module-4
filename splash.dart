import 'dart:html';

import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'main.dart';

class SplashAnimation extends StatefulWidget {
  SplashAnimation({Key? key}) : super(key: key);

  @override
  State<SplashAnimation> createState() => _SplashAnimationState();
}

class _SplashAnimationState extends State<SplashAnimation> {
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Icons.home_rounded,
        splashTransition: SplashTransition.fadeTransition,
        nextScreen: LoginPage());
  }
}
